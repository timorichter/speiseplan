#!/usr/bin/env python
# coding=utf-8
import sys, argparse, json
from parser import *
from view import *
from model import DateWrapper, Gericht

def mkdirs(path):
    """Make directory, if it doesn't exist."""
    if not os.path.exists(path):
        os.makedirs(path)

class Main(object):
    jsonfile="gerichte.json"

    def argparser(self):
        parser = argparse.ArgumentParser(description='Mensacrawler. Schreibt die Ausgabe in ./html/*.html')
        #parser.add_argument("parameter", type=str, help='text')
        parser.add_argument("-u",'--update', default=False, action='store_true', help='Aktualisiere nur Daten von heute')
        parser.add_argument("-c",'--cache', default=False, action='store_true', help='Bevorzuge HTML-Seiten aus cache/ anstatt vom Server')
        parser.add_argument("-r",'--rewrite', default=False, action='store_true', help='Schreibe Ausgabe erneut ohne herunterzuladen')
        parser.add_argument("-v",'--verbose', default=False, action='store_true', help='Fehlerausgabe')
        self.args = parser.parse_args()
        
    def print_(self, x): self.args.verbose and sys.stderr.write(x)

    def _loadJson(self):
        if not os.path.exists(self.jsonfile):
            raise Exception("JSON-Datei existiert nicht. Verwende andere Parameter.\n")
            #return self._fetchall()
        else:
            self.print_("Lade jsonfile\n")
            with open(self.jsonfile) as f: 
                gerichte = json.load(f)
                gerichte = [Gericht.fromDict(x) for x in gerichte]
            return gerichte
    
    def _update(self, gerichte):
        """ aktualisieren der Gerichte von heute """
        # remove all from today in gerichte
        today = datetime.date.today()
        gerichte = [g for g in gerichte if g.datum != today]
        
        # retrieve all gerichte from today
        self.print_("Filter\n")
        gerichte=Filter()(gerichte)
        self.print_("Lade Übersicht\n")
        stw_today = DateWrapper._todayToStwDate()
        gerichte_neu = ParserUebersicht(self.print_)(*stw_today)
        gerichte_neu=self._ladeDetails(gerichte_neu)
        gerichte.extend(gerichte_neu)
        return gerichte
        
    def _ladeDetails(self,gerichte):
        """
        Präfilter und herunterladen der einzelnen Detailansichten
        """
        self.print_("Filter\n")
        gerichte=Filter()(gerichte)
        for x,g in enumerate(gerichte):
            self.print_(
                "\rLade Gericht (%d/%d)"% \
                (x+1,len(gerichte)) )
            ParserGerichtPlus(self.print_)(g)
        self.print_("\n")
        return gerichte
        
    def _fetchall(self):
        """ Herunterladen aller Gerichte """
        gerichte=[]
        self.print_("Lade Übersicht\n")
        # TODO: crawle nur die nächsten 2-3 tage: week 0, montag=day1
        # ranges: day: [0..6], week: [0..2]
        for day in range(0,6+1):
            for week in range(0,1+1):
                gerichte.extend(ParserUebersicht(self.print_)(week,day))
        gerichte=self._ladeDetails(gerichte)
        return gerichte

    def __init__(self):
        self.argparser()
        if self.args.cache:
            Web.getSource=Web._cache # enable website caching

        if self.args.rewrite: gerichte = self._loadJson()
        elif self.args.update: 
            gerichte = self._loadJson()
            gerichte = self._update(gerichte)
        else: gerichte = self._fetchall() 

        gerichteDict = [x.toDict() for x in gerichte]
        with open(self.jsonfile,"wb") as f:
            json.dump(gerichteDict,f)
        
        mkdirs("html")
        self.print_("Filter\n")
        gerichteVegan=Vegan()(gerichte)
        gerichteGluten=Glutenfilter()(gerichte)
        gerichteVeganGluten=Glutenfilter()(gerichteVegan)
        self.print_("View\n")
        v = VeganView()(gerichteVegan)
        with open("html/vegan.html","w") as f: 
            f.write(v.encode("utf-8"))
        v = VeganAltView()(gerichteVegan)
        with open("html/alt.html","w") as f: 
            f.write(v.encode("utf-8"))
        with open("html/index.html","w") as f: 
            f.write(v.encode("utf-8"))
        v = GlutenView()(gerichteGluten)
        with open("html/glutenfrei.html","w") as f:
            f.write(v.encode("utf-8"))
        v = VeganGlutenView()(gerichteVeganGluten)
        with open("html/glutenfreivegan.html","w") as f:
            f.write(v.encode("utf-8"))
        

if __name__ == '__main__':
    Main()


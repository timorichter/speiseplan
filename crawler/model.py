# coding=utf-8

import datetime, wrapper

try:
    import ntplib
    c = ntplib.NTPClient()
    response = c.request('de.pool.ntp.org', version=3)
    #DATE_TODAY = time.gmtime(response.tx_time)
    DATE_TODAY = datetime.date.fromtimestamp(response.tx_time)
except:
    DATE_TODAY = datetime.date.today()

class DictObject(dict):
    @classmethod
    def fromDict(cls, d, *args, **xargs):
        # äquivalent zu return cls(d) und kein Ausführen von __init__()
        c = cls(*args, **xargs)
        c.update(d)
        return c
        
    def toDict(self):
        return dict(self)

    def __getattr__(self,name): 
        return self[name]

    def __setattr__(self,name,value): 
        self[name]=value
        
    

class Gericht(DictObject):
    """
    Attribute:
    {
        mensa = String
        preis = String
        name = String
        stoffe = [String]
        href = String
        datum = Integer in der Form wd = Woche (0-9), Tag(0-6)
        datumInt = Integer YYYYmmddHHMMSS
        week = Integer 0..2
        day = Integer 0..6
        foto = String url
        thumb = String url
    }
    """
    def __init__(self):
        self["mensa"] = ""
        self["preis"] = ""
        self["href"] = ""
        #self["datum"] = datetime.date(2000,01,01)
        self["name"] = ""
        self["foto"] = None
        self["thumb"] = None
        self.stoffe=[]
        
    @property
    def datumInt(self):
        return self._dateToInt(self.datum)
        
    def _dateToInt(self,date):
        return int(date.strftime("%Y%m%d%H%M%S"))
        
    @property
    def ausverkauft(self):
        return "AUSVERKAUFT" in self.preis.upper()
        
    @property
    def datum(self):
        return DateWrapper(self.week,self.day)
        
    def _realUrl(self, url):
        #if url.startswith("//"): url = "http:%s"%url
        return url
    
    @property
    def foto(self):
        url = super(Gericht, self).foto
        return self._realUrl(url)
        
    @property
    def thumb(self):
        return self._realUrl(super(Gericht, self).foto)

    def __str__(self):
        return "(%s) %s. %s"%(self.mensa,self.name,self.preis)

class DateWrapper(wrapper.Wrapper):
    """
    Parser für das Datum nach der Zählweise des Studentenwerks.
    Dies ist ein wrapper für datetime.datetime
    """

    def __init__(self,week,day):
        self.ordnungszahl = DATE_TODAY.toordinal()

        self.date = self.getDate(week,day)
        super(DateWrapper,self).__init__(self.date)
            
    def _fromStwDate(self,day):
        """ Konvertiert die Zahl eines Tags von der
        Studentenwerkzählweise zur python-Zählweise """
        return (day-1)%7
        
    def _stwWeekAndDayToDays(self,week,day):
        """ Konvertiert eine Wochen- und Tagesanzahl zu einer 
        absoluten Tagesanzahl """
        return week*7+self._fromStwDate(day)
        
    @classmethod
    def _todayToStwDate(self):
        """ Gibt das heutige Datum als Tupel (week,day) nach
        der Zählweise des Studentenwerks aus """
        day=DATE_TODAY.weekday()+1
        if day==7:
            week=-1 # workaround für bug im studentenwerk, sonst 0
            day=0
        else: week=0
        return (week,day)
        
    def getDate(self,week,day):
        """
        Konvertiert (week,day) der Zählweise des Studentenwerks
        zum python-Datum
        """
        differenz = self._stwWeekAndDayToDays(week,day)-self._stwWeekAndDayToDays(*self._todayToStwDate())
        date = datetime.date.fromordinal(self.ordnungszahl+differenz)
        #TODO: date-datetime.timedelta(days=int) benutzen
        return date

# FILTERS
class _abstractFilter(object):
    def __call__(self, gerichte): return gerichte

class Filter(_abstractFilter):
    """
    Filter für ungültige Einträge im Speiseplan
    """
    def __call__(self,gerichte):
        """
        Entfernt aus der Liste Gerichte von gestern, vorgestern, etc.
        """
        self.gerichte=gerichte
        self.entferneLeere()
        self.entferneVergangene()
        self.gerichte = [self.checkMensaname(g) for g in self.gerichte]
        return self.gerichte

    def entferneLeere(self):
        gerichte=[]
        for g in self.gerichte:
            #if g.name != "" and g.href=="": print g # „Kein Angebot“
            if g.name != "" and g.href != "":
                gerichte.append(g)
        self.gerichte=gerichte

    def entferneVergangene(self):
        today = DATE_TODAY
        gerichte=[]
        for g in self.gerichte:
            if g.datum.date >= today:
                gerichte.append(g)
        self.gerichte=gerichte

    def checkMensaname(self, g):
        """ Benennt BioMensa Uboot (BioNr blabla) um in BioMensa UBoot """
        if "(" in g.mensa: g.mensa = g.mensa.split("(")[0].strip()
        return g
        
class Glutenfilter(_abstractFilter):
    def __call__(self,gerichte):
        result=[]
        for g in gerichte:
            if "GLUTENFREI" in g.name.upper() \
            or "BUCHWEIZEN" in g.name.upper() \
            or ("A" not in g.stoffe \
            and "PASTA:" not in g.name.upper() ):
                result.append(g)
        return result

class Vegan(_abstractFilter):
    """
    Filter für vegane Gerichte
    """
    def __call__(self,gerichte):
        #super(Vegan,self).__call__(gerichte)
        self.gerichte=gerichte
        self.findeVegan()
        return self.gerichte

    def findeVegan(self):
        gerichte=[]
        for g in self.gerichte:
            if "VEGAN" in g.name.upper() \
            or "VEGAN" in ",".join(g.stoffe).upper() \
            or "G" not in g.stoffe \
            and "C" not in g.stoffe \
            and "B" not in g.stoffe \
            and "N" not in g.stoffe \
            and "Fleisch" not in g.stoffe \
            and u'HÄHNCHEN' not in g.name.upper() \
            and u'HÜHNCHEN' not in g.name.upper() \
            and u'HÜHNER' not in g.name.upper() \
            and u'CHICKEN' not in g.name.upper() \
            and u'HUHN' not in g.name.upper() \
            and u'LAMM' not in g.name.upper() \
            and u'SPECK' not in g.name.upper() \
            and u'GEFLÜGEL' not in g.name.upper() \
            and u'ENTE' not in g.name.upper() \
            and u'PUTE' not in g.name.upper() \
            and "D" not in g.stoffe: # Fisch
                g.stoffe=[x for x in g.stoffe if "VEGAN" not in x.upper()]
                gerichte.append(g)
        self.gerichte = gerichte



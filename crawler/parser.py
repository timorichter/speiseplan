# coding=utf-8

import urllib2, re, os
import libxml2dom

from model import *

class Web(object):
    """ common web retrieval functions """
    
    def __init__(self, print_):
        self.print_=print_
    
    def _cache(self,url):
        urlBase = os.path.basename(url)
        script=os.path.realpath(__file__)
        scriptPath=os.path.dirname(script)
        cache=os.path.join("cache",urlBase.replace("/","_"))
        if os.path.exists(cache): 
            #self.print_("From cache: %s\n"%url)
            self.print_(" (from cache)")
            f = open(cache,"r") # was: with
            r= f.read()
            f.close()
            return r
        else:
            f = open(cache,"w") # was: with
            r = self._getSource(url)
            f.write(r)
            f.close()
            return r
        
    def _getSource(self,url):
        try:
                f=urllib2.urlopen(url)
        except:
                try:
                        f=urllib2.urlopen(url)
                except:
                        f=urllib2.urlopen(url)
        s=f.read()
        f.close()
        return s
    
    getSource=_getSource


class Parser(Web):
    #def getSource(self,filename):
    #    self.source = super(Parser,self).getSource(self.url)

    def buildDom(self):
        try:
            self.dom = libxml2dom.parseString(self.source,html=1)  # from_unicode()
        except:
            self.print_("Error parsing %s\n"%self.url)
            return False

    def getText(self,o): return o.textContent
    
    def __getText(self,o):
        data = ""
        for node in o.childNodes:
            try:
                if node.data:
                    data = data+node.data
            except (AttributeError):
                data = data+self.getText(node)
        return data

class ParserGericht(Parser):
    _url="%s?&print=1"

    def __init__(self,*args,**xargs): 
        super(ParserGericht,self).__init__(*args,**xargs)
        self.stoffe=[]

    def __call__(self,g): 
        """
        Versieht das Gericht @g anhand seiner URL mit Details.
        """
        self.setGericht(g)
        self.loadFromWeb(g["href"])
        self.parse()
        return self.g

    def loadFromWeb(self,filename):
        self.url = self._url%filename
        self.source = self.getSource(self.url)

    def setGericht(self,g): self.g = g
    
    def addFoto(self):
        imgList = self.dom.xpath("//a[@id='essenfoto']/img")
        hrefList = self.dom.xpath("//a[@id='essenfoto']")
        if len(imgList)==0: return
        img=imgList[0].getAttribute("src")
        href=hrefList[0].getAttribute("href")
        self.g.foto = href
        self.g.thumb = img

    def parse(self):
        if self.buildDom() == False: return
        self.addFoto()
        lists= self.dom.getElementsByTagName("ul")
        for l in lists:
            if l.getAttribute("class") != "speiseplaninfos": continue
            listItems = l.getElementsByTagName("li")
            for li in listItems: 
                self.addStoff(li)
        #self.g["stoffe"] = ", ".join(self.stoffe)
        self.g["stoffe"] = self.stoffe

    def addStoff(self,li):
        text = self.getText(li)
        stoffe = re.findall(".*\(([^\)]*)\)",text) #[^\(\)]*
        self.stoffe.extend(stoffe)

class ParserGerichtPlus(ParserGericht):
    """
    Beim parsen: Kennzeichnungen „Fleisch“ und „Vegan“ erkennen
    todo: Klasse verschieben zu filter
    """
    def addStoff(self,li):
        """
        Parser für die Liste der gekennzeichneten Inhaltsstoffe
        """
        super(ParserGerichtPlus,self).addStoff(li)
        self._detectFleisch(li)
        self._detectVegan(li)

    def _detectFleisch(self,li):
        text = self.getText(li) # Inhaltsstoff, z.b. „G“ oder „Rindfleisch“
        f = "Fleisch"
        if "Schweinefleisch" in text or "Rindfleisch" in text: 
            self.stoffe.append(f)
            self.stoffe.append(text)
        

    def _detectVegan(self,li):
        text = self.getText(li) # Inhaltsstoff, z.b. „G“ oder „Rindfleisch“
        if "VEGAN" in text.upper(): self.stoffe.append("Vegan")

class ParserUebersicht(Parser):
    def __init__(self, *args, **xargs):
        super(ParserUebersicht,self).__init__(*args,**xargs)
        self.gerichte = []

    def __call__(self,week,day):
        """
        Lädt eine detaillose Übersicht der Gerichte von der Webseite
        des Studentenwerks und gibt sie als Liste vom Typ Gericht
        zurück.
        """
        self.week=week
        self.day=day
        self.loadFromWeb()
        self.parse()
        return self.gerichte

    _url="http://www.studentenwerk-dresden.de/mensen/speiseplan/w%s-d%s.html"

    def loadFromWeb(self):
        self.url = self._url % (self.week,self.day)
        self.source = self.getSource(self.url)

    def parse(self):
        if self.buildDom() == False: return
        tables = self.dom.getElementsByTagName("table")
        for table in tables:
            self.parseMensa(table)

    def parseMensa(self,table):
        head = table.getElementsByTagName("thead")[0]
        mensa = ""
        for th in head.getElementsByTagName("th"):
            if th.getAttribute("class") == "text":
                mensa = self.getText(th)
                mensa = mensa.replace("Angebote","").strip()
                break
        for body in table.getElementsByTagName("tbody"):
            for tr in body.getElementsByTagName("tr"):
                g = Gericht()
                g.mensa = mensa
                g.week = self.week
                g.day = self.day
                #g.datum = DateParser(self.week,self.day)
                if self.parseGericht(g,tr):
                    self.gerichte.append(g)
                

    def parseGericht(self,g,tr):
        for td in tr.getElementsByTagName("td"):
            cs = td.getAttribute("class")
            if cs == "text": 
                g["name"] = self.getText(td)
            if cs == "preise": g["preis"] = self.getText(td)
        for a in tr.getElementsByTagName("a"):
            href = a.getAttribute("href")
            if "details" not in href: return False
            if href != "":
                g["href"] = \
                    os.path.join(os.path.dirname(self.url),href)
                break
        return True


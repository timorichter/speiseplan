class Wrapper(object):
    '''
    Object wrapper class.
    This a wrapper for objects. It is initialiesed with the object to wrap
    and then proxies the unhandled getattribute methods to it.
    Other classes are to inherit from it.
    '''
    def __init__(self, obj):
        '''
        Wrapper constructor.
        @param obj: object to wrap
        '''
        # wrap the object
        self._wrapped_obj = obj

    def __getattr__(self, attr):
        # see if this object has attr
        # NOTE do not use hasattr, it goes into
        # infinite recurrsion
        if attr in self.__dict__:
            # this object has it
            return getattr(self, attr)
        # proxy to the wrapped object
        return getattr(self._wrapped_obj, attr)

    def __eq__(self,other): return self._wrapped_obj == other
    def __ne__(self,other): return self._wrapped_obj != other
    def __lt__(self,other): return self._wrapped_obj < other
    def __le__(self,other): return self._wrapped_obj <= other
    def __gt__(self,other): return self._wrapped_obj > other
    def __ge__(self,other): return self._wrapped_obj >= other


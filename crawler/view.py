# coding=utf-8

import locale

class View(object):
    """
    Abstrakte Darstellungsklasse
    """
    
    def __init__(self):
        try:
            locale.setlocale(locale.LC_ALL, "german")
        except locale.Error:
            locale.setlocale(locale.LC_ALL,"de_DE.utf8")

    def datumToStr(self,datum):
        return datum.strftime("%A, %d. %B").decode("latin-1")

    def datumToStrKurz(self,datum):
        return self.datumToStr(datum)
        return datum.strftime("%A").decode("latin-1")
        
    def __call__(self,gerichte):
        for g in gerichte:
            stoffe = ", ".join(g["stoffe"])
            datum = self.datumToStr(g)
            out= "%s: %s [%s] %s, %s.\n" % \
                (datum, g["name"],stoffe,g["preis"],g["mensa"])
            #print g
            print out.encode("utf-8")

class _Gruppierer(object):
    """
    Gruppiert eine Liste von Gerichten nach einem
    Attribut.
    """
    
    def sortedDict(self,adict):
        """
        @adict Dict
        @return Ordered List of Tuples. [(key,value)]
        """
        items = adict.items() # {a:b, ...} => [(a,b), ...]
        items.sort() # sort by key
        return items
        #return [(key,value) for key, value in items]
    
    def groupGerichte(self,gerichte,groups):
        """
        @gerichte Liste mit dem datetime.date-Attribut datum
        @group Liste von gruppen. len(group) = len(gerichte)
        @return Dict mit der Syntax {datetime.date: List}
        """
        group = {}
        for g,g_group in zip(gerichte,groups):
            if g_group not in group: group[g_group] = []
            group[g_group].append(g)
        return group
        
    def groupedAndSorted(self, gerichte, groups):
        """
        @return Ordered List of Tuples. [(key,value)]
        """
        groups = self.groupGerichte(gerichte,groups)
        return self.sortedDict(groups)

import operator
class GruppiererMensa(_Gruppierer):
    reihenfolge=["zeltsch","neue Mensa","Alte Mensa","Siedepunkt","Boot"]

    def _mensaPos(self,x):
        mensa,gs = x
        for x,mensaX in enumerate(self.reihenfolge):
            if mensaX.upper() in mensa.upper():
                return x-len(self.reihenfolge)
        return mensa

    def sortedDict(self, adict):
        return sorted(adict.items(), key=self._mensaPos )
        
    def __call__(self,gerichte):
        mensen = [g.mensa for g in gerichte]
        return self.groupedAndSorted(gerichte,mensen)

class GruppiererDatum(_Gruppierer):
    """
    Gruppiert eine Liste von Gerichten nach ihrem
    Attribut Datum.
    """
    def _dateToInt(self,date):
        return int(date.strftime("%Y%m%d%H%M%S"))
    def _dateFromInt(self,date): #erst ab python2.5
        return datetime.datetime.strptime(str(date),"%Y%m%d%H%M%S")
        
    def __call__(self,gerichte):
        groups=[]
        for g in gerichte: groups.append(self._dateToInt(g.datum))
        gs = self.groupedAndSorted(gerichte,groups)
        return gs[:8] # nur die nächsten X tage

import datetime
class HTMLView(View):
    """
    HTML-Code-Generator
    """
    CSS="custom_neu.css"
    
    def __call__(self,gerichte):
        gruppen1 = GruppiererDatum()(gerichte)
        s= '''
            <html>
                <head>
                    <!-- CREATED ON %s -->
                    <META HTTP-EQUIV="EXPIRES" CONTENT="-1">
                    <META HTTP-EQUIV="PRAGMA" CONTENT="NO-CACHE">
                    <META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                    <meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=no' />
                    <link href="lightbox/css/lightbox.css" rel="stylesheet">
                    <link rel="stylesheet" href="smoothtaste.css" type="text/css">
                    <link rel="stylesheet" href="custom_common.css" type="text/css">
                    <link rel="stylesheet" href="%s" type="text/css">
                    <link rel="icon" type="image/png" href="fork-48.png" sizes="48x48">
                    <title>Der Speiseplan</title>
                    
                    <!-- Global site tag (gtag.js) - Google Analytics -->
                    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119158203-1"></script>
                    <script>
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());
                    
                      gtag('config', 'UA-119158203-1');
                    </script>

                </head>
                <body>
                <img class="counter" style="display:none" src="counter.php"/>
                <div class="inhalt">
                '''%(unicode(datetime.datetime.now()),self.CSS)

        for x,(datum,gerichte1) in enumerate(gruppen1):
            s+="<a id='%d'></a>"%x
            s+=self._makeNav(gruppen1,x)
            if x == 0:
                s+='''
                    <header>
                        <h1>Der Speiseplan</h1>
                        <h3>%s</h3>
                    </header>
                '''%self.TITEL
            s+="<section><h2 class='datum'>%s</h2>"%(self.datumToStr(gerichte1[0].datum))
            if x==0:
                s+=self.teaser(gerichte1)
                #s+="<a id='%d'></a>"%x
            s+=self.viewSection(gerichte1)
            s += "</section>"
        # footer:
        s+=u"""<footer id='footer'>
            <h3>Feedback an <a href='mailto:&#115;&#053;&#049;&#049;&#056;&#049;&#052;&#048;&#064;&#109;&#097;&#105;&#108;&#046;&#122;&#105;&#104;&#046;&#116;&#117;&#045;&#100;&#114;&#101;&#115;&#100;&#101;&#110;&#046;&#100;&#101;'>&#115;&#053;&#049;&#049;&#056;&#049;&#052;&#048;&#064;&#109;&#097;&#105;&#108;&#046;&#122;&#105;&#104;&#046;&#116;&#117;&#045;&#100;&#114;&#101;&#115;&#100;&#101;&#110;&#046;&#100;&#101;</a><br/>
            Favicon is provided by icons8 as Creative Commons Attribution-NoDerivs 3.0 Unported.<br/>
            Quelle und Fotos: Studentenwerk Dresden<br/>
            <br/>
            Weitere Speisepläne:<br/>
            <ul>
                <li><a href='glutenfreivegan.html'>Glutenfrei + vegan</a></li>
                <li><a href='glutenfrei.html'>Glutenfrei</a></li>
                <li><a href='vegan.html'>Vegan, neues Layout</a></li>
                <li><a href='alt.html'>Vegan, altes Layout</a></li>
                <li><a href='http://www.studentenwerk-dresden.de/mensen/speiseplan/'>Speiseplan des Studentenwerks</a></li>
            </ul>
            </h3></footer>
            <script src="lightbox/js/lightbox-plus-jquery.js"></script>
            <script type="text/javascript" src="smoothscroll/smoothscroll.js"></script> """
        
        s+="</div></body></html>"
        return s

    def _makeNav(self, gruppen1, x):
                """
                x: make navigation for position x
                gruppen1: [(datum, ?)]
                x2: position, iterator
                wt: zeige rechts und links je @wt weitere elemente
                """
                wt=1
                letztes = len(gruppen1)-1
                nav="<nav><table><tr>"
                vorherigesElement = x -1*wt -1 - (wt-min(wt, letztes-x))
                if x > 0:
                    nav+="<td><a class='smoothscroll' href='#%d'>%s</a></td>"%(max(0,vorherigesElement),u"‹")
                for x2,(datum2,y) in enumerate(gruppen1):
                    # nur die nächsten und vorherigen wt elemente darstellen.
                    # sind nach links weniger als wt elemente vorhanden,
                    # dann werden mehr elemente rechts dargestellt und umgekehrt.
                    if x2 > x+wt + (wt-min(wt, x)): continue
                    if x2 < x-wt - (wt-min(wt, letztes-x)): continue
                    linktext = self.datumToStrKurz(y[0].datum)
                    if x==x2:
                        nav+="<td><a class='smoothscroll' xhref='#%d'><b>%s</b></a></td>"%(x2,linktext)
                    else: nav+="<td><a class='smoothscroll' href='#%d'>%s</a></td>"%(x2,linktext)
                naechstesElement = x+1*wt+1+(wt-min(wt, x))
                if x < letztes:
                    nav+="<td><a class='smoothscroll' href='#%d'>%s</a></td>"%(min(letztes,naechstesElement),u"›")
                nav+=u"<td><a class='smoothscroll' href='#footer'>Über</a></td>"
                nav+="</tr></table></nav>" 
                return nav
                   
    def viewSection(self,gerichte1):
            s=""
            for mensa,gerichte2 in GruppiererMensa()(gerichte1):
                s+="<article class='mensagerichte'><h2>%s</h2>"%mensa
                s+=self.viewMensa(gerichte2)
                s+="</article>"
            return s

    def teaser(self,gerichte):
        ausgewaehlt=False
        for g in gerichte:
            if g.foto and not g.ausverkauft: 
                ausgewaehlt=True
                break
        if not ausgewaehlt: return ""
        return "<article class='teaser'><table><tr><td><a data-lightbox='%s' data-title='%s' href='%s'><div class='imgdiv'><img src='%s'></div><div class='textdiv'>%s<br/><br/>%s<br/><br/>%s</div></a></td></tr></table></article>"%(g.datumInt,unicode(g),g.foto,g.foto, g.name, g.preis, g.mensa)
        
    def viewMensa(self,gerichte):
        s="<table><tr><th class='name'></th><th class='stoffe'>Inhalt</th><th class='preis'>Preis</th><th class='mensa'></th></tr>"
        for g in sorted(gerichte):
            stoffe = ", ".join(g.stoffe)
            s += "<tr><td class='name'>"
            if g.foto: s+="<a data-lightbox='%s' data-title='%s' href='%s' class='imgdiv'><img src='%s'/></a>"%(g.datumInt,unicode(g),g.foto,g.thumb)
            s+="<a href='%s'>%s</a></td><td class='stoffe'><a href='%s'>%s</a></td><td class='preis'><a href='%s'>%s</a></td><td class='mensa'><a href='%s'>%s</a></td></tr>"%(g.href,g.name,g.href,stoffe,g.href,g.preis,g.href,g.mensa)
        s += "</table>"
        return s

class VeganView(HTMLView):
    TITEL="Veganes Essen in der Mensa!"

class VeganAltView(VeganView):
    CSS="custom_alt.css"
    def viewSection(self,gerichte1):
            s="<article class='mensagerichte'>"
            s+="<table><tr><th class='name'></th><th class='stoffe'>Inhalt</th><th class='preis'>Preis</th><th class='mensa'></th></tr>"
            for mensa,gerichte2 in GruppiererMensa()(gerichte1):
                for g in gerichte2:
                    stoffe = ", ".join(g.stoffe)
                    preis = "<nobr>%s</nobr>"%g.preis.replace(" / ","</nobr> / <nobr>")
                    s += "<tr><td class='name'>"
                    s+="<a href='%s'><div class='mensatext'>(%s) </div>%s</a></td><td class='stoffe'><a href='%s'>%s</a></td><td class='preis'><a href='%s'>%s</a></td><td class='mensa'><a href='%s'>%s</a></td></tr>"%(g.href,g.mensa,g.name,g.href,stoffe,g.href,preis,g.href,g.mensa)
            s += "</table>"
            s+="</article>"
            return s
    
    
class GlutenView(HTMLView):
    TITEL="Glutenfrei"

class VeganGlutenView(HTMLView):
    TITEL="Glutenfrei + vegan"
    



# Mensa crawler für vegane Gerichte des Studentenwerks Dresden.

## 1. INSTALLIEREN:

Das Verzeichnis „html“ sollte ggf. öffentlich lesbar sein, z. B.
```bash
mv html ~/public_html/mensa
ln -s ~/public_html/mensa html
chmod -R a+rX ~/public_html/mensa
```

### 1.1 Für cron:
```bash
echo 11 03 * * *        `pwd`/fetch.sh | crontab -e
echo */10 11-15 * * *        `pwd`/fetch.sh -u | crontab -e
```

### 1.2 Oder für standalone daemon:

```bash
$ daemon/run.sh
```

## 2. Pfadhierarchie

| Pfad | Bedeutung |
| - | - |
| mensa/html/                | HTML-Output | 
| mensa/daemon/run.sh         | Standalone daemon, der regelmäßig den Output aktualisiert |
| mensa/fetch.sh              | Aktualisiert den Output einmalig, sollte z. B. von Cron aufgerufen werden |
| mensa/crawler/control.py     | Starte Crawler |




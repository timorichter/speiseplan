#!/bin/bash
cd "`dirname $0`"
source variables.sh

if [ -e "$PIDFILE" ]; then
    pid="$(cat "$PIDFILE"|head -n1)"
    rm "$PIDFILE"
fi

if ! [ -e "$PIDFILE" ] || ! ps $pid >>/dev/null; then
    echo Daemon not running!
    exit 1
else
    kill $pid
    echo Daemon stopped.
fi


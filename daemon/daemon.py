#!/usr/bin/python
import schedule
import time,subprocess

def fetchall():
    subprocess.Popen(["../fetch.sh"])

def update():
    subprocess.Popen(["../fetch.sh","-u"])
    

for minute in xrange(00,60,10): # 00-60 each 10 minutes
    for hour in xrange(11,15):
        schedule.every().day.at("%02d:%02d"%(hour,minute)).do(update)

schedule.every().day.at("03:00").do(fetchall)

while True:
    schedule.run_pending()
    time.sleep(10)
    

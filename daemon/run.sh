#!/bin/bash
cd "`dirname $0`"
source variables.sh

if [ -e "$PIDFILE" ]; then
    oldpid="$(cat "$PIDFILE"|head -n1)"
    if ps $oldpid >>/dev/null; then
        echo Old process $oldpid still running.
        exit 1
    fi
fi

echo Starting daemon.py
nohup python daemon.py 2>&1 >>/dev/null&
echo $! > "$PIDFILE"

